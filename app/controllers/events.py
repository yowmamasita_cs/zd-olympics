from ferris import Controller, messages, route_with
from app.models.event import Event
import json


class Events(Controller):
    class Meta:
        Model = Event
        components = (messages.Messaging,)
        prefixes = ('api',)

    @route_with('/api/events', methods=['POST'])
    def api_create(self):
        params = json.loads(self.request.body)
        event = Event.create(params)
        self.context['data'] = event

    @route_with('/api/events/:<key>', methods=['POST'])
    def api_update(self, key):
        params = json.loads(self.request.body)
        event = self.util.decode_key(key).get()
        event.update(params)
        self.context['data'] = event

    @route_with('/api/events/:<key>', methods=['DELETE'])
    def api_delete(self, key):
        event = self.util.decode_key(key).get()
        event.delete()
        return 200

    @route_with('/api/events/list')
    def api_list(self):
        self.context['data'] = Event.list()

    @route_with('/api/events/list:<sport>')
    def api_list_by_sport(self, sport):
        self.context['data'] = Event.list_by_sport(sport)

    @route_with('/api/events/list/upcoming')
    def api_list_upcoming(self):
        """Display events which are coming up."""
        self.context['data'] = Event.list_upcoming()

    @route_with('/api/events/list/upcoming:<sport>')
    def api_list_upcoming_by_sport(self, sport):
        """Display events which are coming up (by sport)."""
        self.context['data'] = Event.list_upcoming_by_sport(sport)

    @route_with('/api/events/in_progress')
    def api_in_progress(self):
        """View the sports that are currently in progress."""
        self.context['data'] = Event.list_in_progress()

    @route_with('/api/events/in_progress:<sport>')
    def api_in_progress_by_sport(self, sport):
        """Displays results from the previous games/races under the current sports."""
        self.context['data'] = Event.list_previous_by_sport(sport)
