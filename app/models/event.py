from ferris import BasicModel, ndb
import datetime


class Event(BasicModel):
    sport = ndb.StringProperty()
    participants = ndb.StringProperty(repeated=True)
    start = ndb.DateTimeProperty()
    end = ndb.DateTimeProperty()
    result = ndb.StringProperty()

    def before_put(self):
        self.sport = self.sport.lower()

    @classmethod
    def create(cls, params):
        item = cls(**params)
        item.put()
        return item

    def update(self, params):
        self.populate(**params)
        self.put()

    def delete(self):
        ndb.delete_multi(self.__class__.query(ancestor=self.key).iter(keys_only=True))

    @classmethod
    def list(cls):
        return cls.query()

    @classmethod
    def list_upcoming(cls):
        return cls.list().filter(cls.start > datetime.datetime.now())

    @classmethod
    def list_in_progress(cls):
        return cls.list().filter(cls.start <= datetime.datetime.now()).filter(cls.end > datetime.datetime.now())

    @classmethod
    def list_previous(cls):
        return cls.list().filter(cls.end <= datetime.datetime.now())

    @classmethod
    def list_by_sport(cls, sport):
        return cls.list().filter(cls.sport == sport.lower())

    @classmethod
    def list_upcoming_by_sport(cls, sport):
        return cls.list_by_sport(sport).filter(cls.start > datetime.datetime.now())

    @classmethod
    def list_in_progress_by_sport(cls, sport):
        return cls.list_by_sport(sport).filter(cls.start <= datetime.datetime.now()).filter(cls.end > datetime.datetime.now())

    @classmethod
    def list_previous_by_sport(cls, sport):
        return cls.list_by_sport(sport).filter(cls.end <= datetime.datetime.now())
